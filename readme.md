# Netent Framework Team - Code Assignment

Small, full stack javascript application that display random outcome generated on the server

### Prerequisites

node v6.1.0
npm 3.8.6 
live-server or any server environemnt


### Running the app
Navigate to server folder and run 
```
 npm install && npm start
```

Then navigate to client and open with live-server or mamp 

## Acknowledgments

* Application is tested on chrome 63.0 and firefox 57
* In order for the app to work on the rest of the browsers I have to use external plugins such as gulp or webpack to use babel as I am using es6 features which are not natively supported.
